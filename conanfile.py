from conans import ConanFile, CMake, tools
import os

class XxhashConan(ConanFile):
    name = "xxhash"
    license = "BSD 2-Clause"
    author = "toge.mail@gmail.com"
    url = "https://bitbucket.org/toge/conan-xxhash"
    homepage = "https://github.com/Cyan4973/xxHash"
    description = "Extremely fast non-cryptographic hash algorithm"
    topics = ("smhasher", "hash", "checksum")
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False], "inline_api": [True, False]}
    default_options = {"shared": False, "inline_api": True}
    generators = "cmake"

    def source(self):
        tools.get(**self.conan_data["sources"][self.version])
        os.rename("xxHash-{}".format(self.version), "xxHash")

    def build(self):
        cmake = CMake(self)
        cmake.definitions["BUILD_SHARED_LIBS"] = self.options.shared
        cmake.definitions["XXHASH_BUILD_XXHSUM"] = False
        cmake.definitions["XXHASH_BUILD_ENABLE_INLINE_API"] = self.options.inline_api
        cmake.configure(source_folder="xxHash/cmake_unofficial")
        cmake.build()

        # Explicit way:
        # self.run('cmake %s/hello %s'
        #          % (self.source_folder, cmake.command_line))
        # self.run("cmake --build . %s" % cmake.build_config)

    def package(self):
        self.copy("xxhash.h", dst="include", src="xxHash", keep_path=False)
        self.copy("xxh_x86dispatch.h", dst="include", src="xxHash", keep_path=False)
        self.copy("*.lib", dst="lib", keep_path=False)
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)
        self.copy("*.dylib", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["xxhash"]

