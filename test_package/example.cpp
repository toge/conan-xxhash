#include <iostream>
#include "xxhash.h"

int main() {
    auto state = XXH64_createState();

    XXH64_freeState(state);
}
