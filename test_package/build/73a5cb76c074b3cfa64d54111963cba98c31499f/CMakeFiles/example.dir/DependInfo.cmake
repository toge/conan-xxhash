# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/toge/src/conan-xxhash/test_package/example.cpp" "/home/toge/src/conan-xxhash/test_package/build/73a5cb76c074b3cfa64d54111963cba98c31499f/CMakeFiles/example.dir/example.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "_GLIBCXX_USE_CXX11_ABI=1"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/home/toge/.conan/data/xxhash/0.8.0/toge/stable/package/7726b2debb773452688cc26ef0c62575096c2aa9/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
